# Résilience des territoires

> Appel à communs

### Fév 2021 - 10 mois - 10 défis - 300 personnes - 30 communs

Initié par l'ADEME avec la participation de l'AFD et l'ANCT

# Organiser la résilience des territoires

## Ces besoins qui appellent à produire ensemble des Communs

> Connaissances, Données, Logiciels, Matériels

### Alimentation, Transport, Énergie, Bâtiment, Aménagement urbain et territorial, Communication, Sécurité...

### #commun #opensource #opendata #creativecommon
