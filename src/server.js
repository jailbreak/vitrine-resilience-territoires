import compression from "compression"
import polka from "polka"
import sirv from "sirv"

import * as sapper from "@sapper/server"
import configData from "../content/config.yml"
import Config from "./models/config.js"

new Config(configData) // Check that config data is valid

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

if (process.env.VITRINE_BASE_PATH) {
	configData.basePath = process.env.VITRINE_BASE_PATH
}
configData.basePath = configData.basePath || "/"

// console.log("configData", JSON.stringify(configData, null, 2))

polka() // You can also use Express
	.use(
		configData.basePath,
		compression({ threshold: 0 }),
		sirv("static", { dev }),
		sapper.middleware({
			session: () => ({ config: configData }),
		}),
	)
	.listen(PORT, (err) => {
		if (err) console.log("error", err)
	})
