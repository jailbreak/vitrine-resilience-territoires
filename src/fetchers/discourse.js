import { parse } from "node-html-parser"
import { ConfigError } from "./errors.js"
import { fetchJson } from "./fetch.js"

export function getSourceRenderType(source) {
	if (source.category) {
		return "cards"
	} else if (source.topic) {
		return "content"
	} else {
		throw new ConfigError({
			message: `Missing "category" or "topic" property in "${source.type}" source`,
			info: { source },
		})
	}
}

export async function fetchCardsPageData(source, config, context) {
	const topicsResults = await fetchCategoryTopics(source, config, context)
	if (!topicsResults || !topicsResults.topic_list || !topicsResults.topic_list.topics) {
		return []
	}
	return topicsResults.topic_list.topics.map((topic) => topicToCardData(source, topic))
}

export async function fetchContentPageData(source, config, context) {
	return topicToContentData(await fetchTopic(source, config, context))
}

export function topicToCardData(source, topic) {
	return {
		counts: [
			{
				count: topic.like_count,
				icon: "favorite", // Cf https://material.io/resources/icons
				title: "Likes",
			},
			{
				count: topic.reply_count,
				icon: "reply", // Cf https://material.io/resources/icons
				title: "Replies",
			},
		],
		id: topic.id,
		imageUrl: topic.image_url,
		html: topic.excerpt,
		title: topic.title,
		url: `${source.url}/t/${topic.id}`,
	}
}

export function topicToContentData(topic) {
	const firstPost = topic.post_stream.posts[0]
	return {
		html: firstPost.cooked,
		title: topic.title,
		raw: false,
	}
}

export async function fetchCategoryTopics(source, config, context) {
	const { url: baseUrl, category, limit } = source
	const url = `${baseUrl}/c/${category}.json`
	let result = null
	for (let page = 0; ; page++) {
		const pageUrl = `${url}?page=${page}`
		const data = await fetchJson(pageUrl, context)
		if (result === null) {
			result = data
		} else {
			result.topic_list.topics = [
				...result.topic_list.topics, // already fetched topics
				...data.topic_list.topics, // newly fetched topics
			]
		}
		if (result.topic_list.topics.length < result.topic_list.per_page || data.topic_list.topics.length === 0) {
			if (config.filterTag) {
				result.topic_list.topics = result.topic_list.topics.filter((topic) => topic.tags.includes(config.filterTag))
			}
			if (source.filterTag) {
				result.topic_list.topics = result.topic_list.topics.filter((topic) => topic.tags.includes(source.filterTag))
			}
			if (limit !== null && result.topic_list.topics.length >= limit) {
				result.topic_list.topics = result.topic_list.topics.slice(-limit)
			}
			return result
		}
	}
}

export async function fetchTopic(source, config, context) {
	const { url: baseUrl, topic } = source
	const url = `${baseUrl}/t/${topic}.json`
	return await fetchJson(url, context)
}
