// tailwind.config.js
module.exports = {
	future: {
		defaultLineHeights: true,
		purgeLayersByDefault: true,
		removeDeprecatedGapUtilities: true,
		standardFontWeights: true,
	},
	theme: {
		extend: {
			colors: {
				primary: "#ffffff",
				secondary: "#8f99cd",
				tertiary: "#e54d09",
			},
		},
	},
	variants: {},
	plugins: [],
}
