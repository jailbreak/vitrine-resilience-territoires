# documentation of Vitrine

To configure a [Vitrine](https://gitlab.com/jailbreak/vitrine) website, only 2 things need to be done :

1 - Edit the file `content/config.yaml` according to the format of the Vitrine version (current: 3)

2 - Add `images` and custom `md` and `html` files if needed in the `static` folder

For the `url` fields , we can either use an absolute url or a relative one that points to the files and folders in `static`

# `config.yaml` format (v3)

_The config file is written in the YAML format. You must remember that indentations are what structures this file, they are important to understand its hierarchy and the way to write your configuration._

The Vitrine configuration file must respect a strict format and fields are case sensitive.

## Global configuration :

```yml
version: 3
title: Website title
navbar: NAVBAR
filterTag: a-tag
pages:
  - PAGES
```

- `version (*)` : Must be set to `3`

- `title` : Allows to set the title of the website.

- `navbar` : Items and look of the navbar (see [NAVBAR](#NAVBAR)).

- `filterTag` : Only the Discourse categories or topics with this tag will be displayed.

- `pages`: List of pages (see [PAGES](#PAGES)).

## NAVBAR

Configuration of `navbar` :

```yaml
title: A title
logo: logo.png
textColor: "#ffffff"
hoverColor: "#1e1e1e"
externalLinks:
  - title: Jailbreak
    url: https://jailbreak.paris
```

All these items are displayed in the `navbar`.

- `title` : Title of the website.
- `logo` : Logo of website.
- `textColor`: Color of text in navbar.
- `hoverColor`: Color of text in navbar on mouse over.
- `externalLinks`: List of buttons linking to external pages.

  Each item of `externalLinks` has 2 mandatory fields :

  - `title` : Text displayed in the button of the external link
  - `url` : URL of the external link

## PAGES

Each item of the list of `PAGES` (a `PAGE`) has the following fields:

```yaml
slug: ""
navbarTitle: Title in navbar
banner:
  classes: w-full max-w-6xl mx-auto
  url: banner.jpeg
title: a title
subtitle: a subtitle
callToActionButton:
  title: Inscrivez-vous
  url: https://www.jailbreak.paris
zones:
  - ZONES
```

- `slug (*)` : Internal name of a page.

  Use the slug `""` for the home page and a different slug for each `PAGE`.

  Must not have spaces.

- `navbarTitle` : If defined, sets a link to this `PAGE` in the `NAVBAR`.

  Should be set for all pages except the home page

- `banner` : Displays an image on the top of the `PAGE` if `banner.url` is set

  - `classes` : Allows to set [Tailwind CSS](https://tailwindcss.com) or custom classes that define how the banner will be displayed.

  - `url (*)` : URL of banner

- `title` : A text on the top of the `PAGE`, displayed after the `banner`.

- `subtitle` : A text on the top of the `PAGE`, displayed after the `title`.

- `callToActionButton` : Allows to set a button after `banner`, `title` and / or `subtitle`.

  - `title (*)` : Text displayed on button.

  - `url (*)` : Target of button.

- `zones`: List of zones (see [ZONES](#ZONES)).

  A `ZONE` allows to configure the blocks composing the `PAGE`.

## ZONES

Each item of the list of `ZONES` (a `ZONE`) has the following fields:

```yaml
title: a title
slug: an-anchor
backgroundImage: a_file.html
source: SOURCE
button: BUTTON
```

- `title` : An optional text displayed on the top of this `ZONE`.

- `slug` : If defined and `title` is defined, enables an anchor on this zone and makes `title` clickable

- `backgroundImage` :

- `source (*)` : Defines where to fetch the content of this `ZONE`.

  Available sources : [SOURCE_DISCOURSE](#SOURCE_DISCOURSE), [SOURCE_GITLAB](#SOURCE_GITLAB), [SOURCE_HTML](#SOURCE_HTML), [SOURCE_MARKDOWN](#SOURCE_MARKDOWN)

- `button` : An optional button displayed afer the `source`.

  Available type of buttons : [BUTTON_WITH_PAGE_SLUG](#BUTTON_WITH_PAGE_SLUG), [BUTTON_WITH_URL](#BUTTON_WITH_URL)

## SOURCE_DISCOURSE

This type of source allows to display content fetched from a [Discourse](https://www.discourse.org/) instance.

There are two types of `SOURCE_DISCOURSE` : [SOURCE_DISCOURSE_WITH_CATEGORY](#SOURCE_DISCOURSE_WITH_CATEGORY), [SOURCE_DISCOURSE_WITH_TOPIC](#SOURCE_DISCOURSE_WITH_TOPIC)

## SOURCE_DISCOURSE_WITH_CATEGORY

This type of `SOURCE_DISCOURSE` allows to display `cards` with the content of the `topics` of a given `category`.

For example, if we want to display 4 cards of `topics` of the `category` https://forum.resilience-territoire.ademe.fr/c/8, we can use this configuration :

```yaml
type: discourse
url: https://forum.resilience-territoire.ademe.fr
category: 8
limit: 4
cardsPerLine: 3
cardsPerLineSm: 2
filterTag: projects
disableBlur: true
disableReadMore: true
```

- `type (*)` : Must be set to `discourse`.

- `url (*)` : Url of a discourse where to fetch data.

  Remember to [enable the CORS on Discourse](https://gitlab.com/jailbreak/vitrine#enable-cors).

- `category (*)` : Id of the discourse category where to fecth data.

  Can be retrieved in `discourse` from url by copying the `id` after the `"/c/"`. For example for https://forum.resilience-territoire.ademe.fr/c/8, the category `id` is `8`

- `limit` : Number of cards to display.

  All cards of a category are displayed by default.

- `cardsPerLine` : Allows to set how many cards are displayed per line on desktop (screen width ⩾ [lg](https://tailwindcss.com/docs/responsive-design#overview))

  Is set to 4 by default.

- `cardsPerLineSm` : Allows to set how many cards are displayed per line on resolution between [sm](https://tailwindcss.com/docs/responsive-design#overview)) and [lg](https://tailwindcss.com/docs/responsive-design#overview)).

  Is set to 3 by default.

  For screen resolution ⩽ [sm](https://tailwindcss.com/docs/responsive-design#overview), 1 card is displayed per line.

- `filterTag` : If set, only the `topics` who have this tag are displayed.

  No `filterTag` is set by default.

- `disableBlur` : If set to `true`, disables blur effect on the end of the content of a card.

  Useful for example in order to display images

  Is set to `false` by default.

- `disableReadMore` : If set to `true`, disables the bottom section of a card (icons and "+ D'INFOS…")

  Is set to `false` by default.

## SOURCE_DISCOURSE_WITH_TOPIC

This type of SOURCE_DISCOURSE allows to display the content of a `topic`.

For example, if we want to display the content of the topic https://forum.resilience-territoire.ademe.fr/t/a-propos/243, we can use this configuration :

```yaml
SOURCE_DISCOURSE: (category xor topic)
  type: discourse
  url: https://forum.resilience-territoire.ademe.fr
  topic: 243
```

- `type (*)` : Must be set to `discourse`.

- `url (*)` : Url of a discourse where to fetch data.

  Remember to [enable the CORS on Discourse](https://gitlab.com/jailbreak/vitrine#enable-cors).

- `topic (*)` : Id of the discourse topic where to fecth data.

  Can be retrieved on `discourse` from url by copying the `id` after the `"/t/TOPIC_TITLE/"`. For example for https://forum.resilience-territoire.ademe.fr/t/a-propos/243, the topic `id` is `243`

## SOURCE_GITLAB

This type of source allows to display projects fetched from [GitLab](https://www.gitlab.org/) (or from a GitLab instance, like https://framagit.org/ for example).

For example, if we want to display 4 cards with `projets` from the `group` https://forum.resilience-territoire.ademe.fr/c/8, we can use this configuration :

```yaml
type: gitlab
url: https://gitlab.com
group: jailbreak
limit: 4
cardsPerLine: 3
cardsPerLineSm: 2
disableBlur: false
disableReadMore: false
```

- `type (*)` : Must be set to `gitlab`.

- `url (*)` : Url of a GitLab instance where to fetch data.

  Remember to [enable the CORS on Discourse](https://gitlab.com/jailbreak/vitrine#enable-cors).

- `group (*)` : Name of the group where to fetch the projects.

For example for https://gitlab.com/jailbreak, the `group` is `jailbreak`

- `limit` : Number of cards to display .

  All cards of a group are displayed by default.

- `cardsPerLine` : Allows to set how many cards are displayed per line on desktop (screen width ⩾ [lg](https://tailwindcss.com/docs/responsive-design#overview))

  Is set to 4 by default.

- `cardsPerLineSm` : Allows to set how many cards are displayed per line on resolution between [sm](https://tailwindcss.com/docs/responsive-design#overview)) and [lg](https://tailwindcss.com/docs/responsive-design#overview)).

  Is set to 3 by default.

  For screen resolution ⩽ [sm](https://tailwindcss.com/docs/responsive-design#overview), 1 card is displayed per line.

- `disableBlur` : If set to `true`, disables blur effect on the end of the content of a card.

  Is set to `false` by default.

- `disableReadMore` : If set to `true`, disables the bottom section of a card (icons and "+ D'INFOS…")

  Is set to `false` by default.

## SOURCE_HTML

This type of source allows to display html content.

There are two types of `SOURCE_HTML` : [SOURCE_HTML_WITH_FILE](#SOURCE_HTML_WITH_FILE), [SOURCE_HTML_WITH_CONTENT](#SOURCE_HTML_WITH_CONTENT)

## SOURCE_HTML_WITH_FILE

`SOURCE_HTML_WITH_FILE` allows to display the html from a local or remote html file.

```yaml
type: html
file: file.html
raw: true
```

- `type (*)` : Must be set to `html`.

- `file (*)` : Path of an html file on the `static` folder or url of a remote html file.

- `raw` : If set to `true`, disables automatic format for this source (usefull for specific needs).

  Set to `false` by default.

## SOURCE_HTML_WITH_CONTENT

`SOURCE_HTML_WITH_CONTENT` allows to display the html set in the field `content`.

```yaml
type: html
content: |
  <div>some html</div>
raw: false
```

- `type (*)` : Must be set to `html`.

- `content (*)` : Contains the html to be displayed.

- `raw` : If set to `true`, disables automatic format for this source (usefull for specific needs).

  Set to `false` by default.

## SOURCE_MARKDOWN

This type of source allows to display rendered [Markdown](https://daringfireball.net/projects/markdown/) files.

```yaml
type: markdown
file: file.md
```

- `type (*)` : Must be set to `markdown`.

- `file (*)` : Path of an markdown file on the `static` folder or url of a remote markdown file.

## BUTTON_WITH_PAGE_SLUG

This type of button links to a [PAGE](#PAGES) with a matching `slug`.

```yaml
title: Inscrivez-vous
slug: defis
```

- `title (*)` : Text of the button.

- `slug (*)` : On click on the button, links to the Vitrine [PAGE](#PAGES) which has the same `slug`.

## BUTTON_WITH_URL

This type of button links to an external url.

```yaml
title: Jailbreak
url: https://jailbreak.paris
```

- `title (*)` : Text of the button.

- `url (*)` : On click on the button, links to this external `url`.
